import MyBlogListItem from './index.module.css';
import React, { Component } from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { Row, Col, Typography, Divider, Avatar, Badge } from 'antd';
import { Link } from 'react-router-dom'
import moment from 'moment';
import momentLocale from 'moment/locale/zh-cn';

const { Title, Paragraph } = Typography;

export default class index extends Component {
  render() {
    moment.updateLocale('zh-cn', momentLocale);
    return (
        <Fragment>
            <Typography>
              <Link to={`/blog/read/${this.props.blog.blogId}`}>
                <Badge count={this.props.blog.newComments}>
                  <Title level={4}>{this.props.blog.title}</Title>
                </Badge>
              </Link>
              <Paragraph ellipsis={{ellipsis: true, rows: 3}}>{this.props.blog.desc}</Paragraph>
            </Typography>
            <Row>
              <Col span={12} onClick={()=>{window.location.href = `/authorpage/${this.props.blog.authorId}`}}>
                <Avatar src={this.props.blog.authorAvatar}/>&nbsp;&nbsp;&nbsp;
                <div className={MyBlogListItem.authorName}>
                  {this.props.blog.authorName}
                </div>
              </Col>
              <Col span={12}>
                <div className={MyBlogListItem.postTime}>
                  {moment(this.props.blog.postTime).fromNow()}
                </div>
              </Col>
            </Row>
            <Divider />
        </Fragment>
    );
  }
}
