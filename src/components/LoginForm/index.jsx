import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd';
import api from '../../request/api';

export default class index extends Component {

    state = {
        loginning: false
    }

    onFinish = (values) => {
        this.setState({loginning: true});
        api.login(values.email,values.password)
            .then(res=>{
                this.setState({loginning: false});
                if(res.code === 200){
                    localStorage.setItem('Authorization', res.data);
                    message.success('登录成功，即将返回',1);
                    setTimeout(() => {
                        this.props.goBack();
                    }, 1000);
                } else {
                    message.error(res.msg);
                }
            })
            .catch(error=>{
                this.setState({loginning: false});
                message.error("网络错误");
            });
    };

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    formLayout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 20,
        },
    }

    render() {
        return (
        <>
            <Form
                name="basic"
                {...this.formLayout}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                autoComplete="off"
            >
                 <Form.Item
                    label="用户邮箱"
                    name="email"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入邮箱！',
                        },
                        {
                            type: 'email',
                            message: '请输入有效的邮箱地址！',
                        }
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="用户密码"
                    name="password"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入密码！',
                        },
                        ()=>({
                            validator(_, value){
                                if(!value){ // 如果没有输入value则直接放行
                                    return Promise.resolve();
                                }
                                if(value.length >= 4 && value.length <= 255){
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('密码的长度必须在4~255之内!'));
                            }
                        })
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="没有账户？"
                    name="signUp"
                >
                    <a {...this.props}>前往注册</a>
                </Form.Item>
                
                <Form.Item
                    wrapperCol={{
                        offset: this.formLayout.labelCol.span,
                        span: this.formLayout.wrapperCol.span,
                    }}
                >
                    <Button type="primary" htmlType="submit" loading={this.state.loginning}>
                    登录
                    </Button>
                </Form.Item>
            </Form>        
        </>
        )
    }
}
