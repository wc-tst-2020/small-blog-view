import React, { Component } from 'react'
import { Chat } from 'react-jwchat'
import { message, Modal } from 'antd';
import uuid from 'react-uuid'
import moment from 'moment';
import momentLocale from 'moment/locale/zh-cn';
import MyImage from '../MyImage'
import api from '../../request/api';
import constant from '../../request/constant';

const upload_url = constant.base_url + '/upload-media';

export default class index extends Component {

    state = {
        contact: {
            id: -1,
            avatar: '//game.gtimg.cn/images/lol/act/img/champion/Jinx.png',
            nickname: '测试-联系人',
        },
        my: {
            id: -2,
            avatar: '//game.gtimg.cn/images/lol/act/img/champion/Jinx.png',
            nickname: '测试-我',
        },
        // msgList: [
        //     {
        //         _id: '75b5bde8f3b9ef7aa904492cb28baf',
        //         date: 1610016880,
        //         user: {
        //             id: 9527,
        //             avatar: '//game.gtimg.cn/images/lol/act/img/champion/Khazix.png',
        //             nickname: '卡兹克',
        //         },
        //         message: {
        //             type: 'text',
        //             content: (<MyImage src={'//game.gtimg.cn/images/lol/act/a20201103lmpwjl/icon-ht.png'}/>),
        //         },
        //     },
        // ],
        msgList: [],
        imgModal: {
            visible: false,
            confirmLoading: false,
            src: '',
            file: null
        }, 
        tokenModal: false
    }

    ws = null;

    getChatList = ()=>{
        // 获取双方聊天记录
        api.getChatList(this.props.contactId)
        .then(res=>{
            if(res.code === 200) {
                var msgList = res.data.map(function(item){
                    return {
                        _id: item.msgId,
                        date: item.sendTime,
                        user: {
                            id: item.fromUser,
                            avatar: item.fromAvatar,
                            nickname: item.fromUserName,
                        },
                        message: {
                            type: 'text',
                            content: item.type === 'image' ?
                            (<MyImage src={item.content}/>) :
                            item.content,
                        },
                    }
                });
                this.setState({msgList});
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{message.error(error);})
    }

    getData = ()=>{
        // 先获取用户自身的信息
        api.getMySimpleInfo()
        .then(res=>{
            if(res.code === 200) {
                var my = {
                    id: res.data.userId,
                    nickname: res.data.userName,
                    avatar: res.data.avatar
                }
                this.setState({my});
                this.connect();
            } else {
                if(res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{message.error(error)});

        var contactId = parseInt(this.props.contactId);
        if(!isNaN(contactId)) {
            // 获取对方的信息
            api.getUserSimpleInfo(contactId)
            .then(res=>{
                if(res.code === 200) {
                    var contact = {
                        id: res.data.userId,
                        nickname: res.data.userName,
                        avatar: res.data.avatar                        
                    }
                    this.setState({contact});
                } else {
                    message.error(res.msg);
                }
            })
            .catch(error=>{message.error(error)})
        }
    }

    // 前往登录
    toLogin = ()=>{
        this.setState({tokenModal: false});
        window.location.href = "/identity"
    }

    // 取消登录
    cancelLogin = ()=>{
        this.setState({tokenModal: false});
    }    

    sendMsg = (msg)=>{
        if(this.ws != null){
            const {contact, my} = this.state;
            var serverMsg = {
                from: {
                    userId: my.id,
                    userName: my.nickname,
                    avatar: my.avatar
                },
                to: contact.id,
                type: 'text',
                content: msg.message.content
            }
            this.ws.send(JSON.stringify(serverMsg));
            const {msgList} = this.state;
            this.setState({msgList: [...msgList,msg]});
        } else {
            message.error("连接服务器失败");
        }
    }

    selectImage = (files)=>{
        if(files && files[0] && files[0].type !== ''){
            var reader  = new FileReader();
            reader.readAsDataURL(files[0]);
            var _this = this;
            reader.onload = function(e) {
                _this.setState({imgModal: {visible: true, confirmLoading: false, src: e.target.result, file: files[0]}});
            };
        } else {
            message.error('不能选择非图片文件');
        }
    }

    snedImage = (src)=>{
        if(this.ws != null){
            const {contact, my} = this.state;
            // 封装消息，并发送给服务器
            var serverMsg = {
                from: {
                    userId: my.id,
                    userName: my.nickname,
                    avatar: my.avatar
                },
                to: contact.id,
                type: 'image',
                content: src
            }
            this.ws.send(JSON.stringify(serverMsg));
            // 封装消息，并展示给用户自己
            var msg = {
                _id: uuid(),
                date: parseInt(moment().format('X')),
                user: my,
                message: {
                    type: 'text',
                    content: (<MyImage src={src}/>),
                }
            }
            const {msgList} = this.state;
            this.setState({msgList: [...msgList,msg]});
        } else {
            message.error("连接服务器失败");
        }
    }

    upLoadImg = ()=>{
        const {imgModal} = this.state;
        this.setState({imgModal: 
            {
                visible: imgModal.visible, 
                confirmLoading: true, 
                src: imgModal.src, 
                file: imgModal.file
            }
        });

        const serverURL = upload_url
        const xhr = new XMLHttpRequest();
        const fd = new FormData()
    
        const successFn = () => {
            // 关闭弹窗 
            this.setState({imgModal: 
                {
                    visible: false,
                    confirmLoading: false,
                    src: '',
                    file: null
                }
            });
            // 处理上传结果
            const res = JSON.parse(xhr.responseText);
            if(res.code === 200){
                this.snedImage(res.data.url);
            } else {
                message.error(res.msg);
            }
        }
    
        const errorFn = () => {
            // 关闭弹窗 
            this.setState({imgModal: 
                {
                    visible: false,
                    confirmLoading: false,
                    src: '',
                    file: null
                }
            });            
            message.error("上传失败");
        }
    
        xhr.addEventListener("load", successFn, false)
        xhr.addEventListener("error", errorFn, false)
        xhr.addEventListener("abort", errorFn, false)
    
        fd.append('file', imgModal.file)
        xhr.open('POST', serverURL, true)
        xhr.setRequestHeader('Authorization',localStorage.getItem('Authorization'));
        xhr.send(fd)
    }

    cancelUpLoad = ()=>{
        this.setState({imgModal: 
            {
                visible: false,
                confirmLoading: false,
                src: '',
                file: null
            }
        });
    }

    render() {
        const {contact, my, msgList, imgModal} = this.state;
        moment.updateLocale('zh-cn', momentLocale);
        return (
        <>
            <Chat
                contact={contact}
                me={my}
                chatList={msgList}
                onSend={(msg) => {this.sendMsg(msg)}}
                onEarlier={() => {}}
                onImage={(files) => {this.selectImage(files)}}
                style={{
                    width: '100%',
                    height: this.props.height,
                }}
            />
            <Modal title="发送图片" 
                visible={imgModal.visible} 
                confirmLoading={imgModal.confirmLoading} 
                onOk={this.upLoadImg} 
                onCancel={this.cancelUpLoad}
                okText="确认"
                cancelText="取消"
            >
                <MyImage src={imgModal.src}/>
                <p>是否确定发送这张图片？</p>
            </Modal>  

            {/* 前往登录的弹窗 */}
            <Modal 
                title="未登录" 
                visible={this.state.tokenModal} 
                onOk={this.toLogin} 
                onCancel={this.cancelLogin}
                okText="确认"
                cancelText="取消"
            >
                <p>您尚未登录，是否前往登录页面？</p>
            </Modal>             
        </>
        )
    }

    connect = ()=>{
        const {my} = this.state;
        this.ws = new WebSocket(`${constant.base_websock_url}/server/chat/${my.id}`);
        this.ws.onmessage = (e) => {
            this.onMessage(e.data);
        };
        this.ws.onerror = (e) => {
            message.error(e.message);
        };
    }

    onMessage = (data)=>{
        const {msgList} = this.state;
        var serverMsg = JSON.parse(data);
        var msg =  {
            _id: serverMsg.id,
            date: serverMsg.date,
            user: {
                id: serverMsg.from.userId,
                avatar: serverMsg.from.avatar,
                nickname: serverMsg.from.userName,
            },
            message: {
                type: 'text',
                content: 
                    serverMsg.type === 'image' ? 
                    (<MyImage src={serverMsg.content}/>) :
                    serverMsg.content,
            },
        }
        this.setState({msgList: [...msgList, msg]});
    }

    componentDidMount(){
        this.getChatList();
        this.getData();
    }
}
