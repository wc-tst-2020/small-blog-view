import BlogReadBar from './index.module.css';
import React, { Component } from 'react';
import { Tooltip, Modal, message } from 'antd';
import { DislikeOutlined, LikeOutlined, DislikeFilled, LikeFilled, EditOutlined, DeleteOutlined, StarOutlined, StarFilled } from '@ant-design/icons';
import api from '../../request/api';

export default class index extends Component {

    state = {
        like: {likeLock: true, likes: 100, dislikes: 0, action: null}
        , collection: {colLock: true, collectionNum: 100, isCollection: false}
        , deleteModal: {visible: false, confirmLoading: false}
        , tokenModal: false
    }

    getData = ()=>{
        var blogId = this.props.blog.blogId;
        // 获取点赞数据
        api.getLikesOfBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                var action = null;
                if (res.data.userPoint !== 0) {
                    action = res.data.userPoint > 0 ? 'liked' : 'disliked';
                }
                var like = {
                    likeLock: true,
                    likes: res.data.likes,
                    dislikes: res.data.dislikes,
                    action: action
                }
                this.setState({like});
            } else {
                console.log(res.msg);
            }
        })
        .catch(error=>{console.log(error)});

        // 获取收藏数据
        api.getCollectionInfo(blogId)
        .then(res=>{
            if(res.code === 200) {
                var collection= {
                    colLock: true, 
                    collectionNum: res.data.count, 
                    isCollection: res.data.collect
                }
                this.setState({collection});
            } else {
                console.log(res.msg);
            }
        })
        .catch(error=>{console.log(error)});
    }

    // 前往登录
    toLogin = ()=>{
        this.setState({tokenModal: false});
        window.location.href = "/identity"
    }

    // 取消登录
    cancelLogin = ()=>{
        this.setState({tokenModal: false});
    }

    // 发送点击喜欢请求
    sendLike = (callBack)=>{
        var blogId = this.props.blog.blogId;
        api.agreeBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                callBack();
            } else {
                if(res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{message.error(error)});
    }

    // 点击喜欢的回调
    like = ()=>{
        const {like} = this.state;
        if(like.likeLock){
            this.setState({like: {likeLock: false, ...like}});
            if(like.action === 'liked'){
                // 取消喜欢
                this.sendLike(()=>{this.setState({like: {likeLock: true, likes: like.likes-1, dislikes: like.dislikes, action: null}});});
            } else {
                // 喜欢
                if (like.action === 'disliked') { // 同时取消不喜欢
                    this.sendLike(()=>{this.setState({like: {likeLock: true, likes: like.likes+1, dislikes: like.dislikes-1, action: 'liked'}});});
                } else {
                    this.sendLike(()=>{this.setState({like: {likeLock: true, likes: like.likes+1, dislikes: like.dislikes, action: 'liked'}});});
                }
            }
        }
    }

    // 发送点击不喜欢的请求
    sendDisLike = (callBack)=>{
        var blogId = this.props.blog.blogId;
        api.disagreeBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                callBack();
            } else {
                if(res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{message.error(error)});
    }

    // 点击不喜欢的回调
    dislike = ()=>{
        const {like} = this.state;
        if(like.likeLock){
            this.setState({like: {likeLock: false, ...like}});
            if(like.action === 'disliked'){
                // 取消不喜欢
                this.sendDisLike(()=>{this.setState({like: {likeLock: true, likes: like.likes, dislikes: like.dislikes-1, action: null}});});
            } else {
                // 不喜欢
                if (like.action === 'liked') { // 同时取消喜欢
                    this.sendDisLike(()=>{this.setState({like: {likeLock: true, likes: like.likes-1, dislikes: like.dislikes+1, action: 'disliked'}});});
                } else {
                    this.sendDisLike(()=>{this.setState({like: {likeLock: true, likes: like.likes, dislikes: like.dislikes+1, action: 'disliked'}});});
                }
            }
        }
    }

    // 发送收藏请求
    sendCollection = (callBack)=>{
        var blogId = this.props.blog.blogId;
        api.collectBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                callBack();
            } else {
                if(res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{message.error(error)});
    }

    // 发送取消收藏请求
    sendDiscollection = (callBack)=>{
        var blogId = this.props.blog.blogId;
        api.disCollectBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                callBack();
            } else {
                if(res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{message.error(error)});
    }    

    // 点击收藏的回调
    collection = ()=>{
        const {collection} = this.state;
        if (collection.colLock) {
            this.setState({collection: {colLock: false, ...collection}});
            if(collection.isCollection) { // 取消收藏
                this.sendDiscollection(()=>{this.setState({collection: {colLock: true, collectionNum: collection.collectionNum-1, isCollection: false}});});
            } else { // 添加收藏
                this.sendCollection(()=>{this.setState({collection: {colLock: true, collectionNum: collection.collectionNum+1, isCollection: true}});});
            }
        }
    }

    // 点击编辑的回调
    edit = ()=>{
        var blog = this.props.blog;
        api.getMySimpleInfo()
        .then(res=>{
            if(res.code === 200) {
                if(blog.authorId === res.data.userId){
                    window.location.href=`/blog/editblog/${blog.blogId}`;
                } else {
                    message.error("不能修改其他用户的博客");
                }
            }
        })
        .catch(error=>{message.error(error)});
    }

    // 获取博客引用的文件列表
    deleteFilesArr = (htmlContent)=>{
        var el = document.createElement("div");
        el.innerHTML = htmlContent;
        var imgs = el.getElementsByTagName("img");
        var videos = el.getElementsByTagName("video");
        var audios = el.getElementsByTagName("audio");
        var fileArr = [];
        for(let i = 0; i < imgs.length; i++){
            fileArr.push(imgs[i].getAttribute("src"));
        }
        for(let i = 0; i < videos.length; i++){
            fileArr.push(videos[i].getAttribute("src"));
            fileArr.push(videos[i].getAttribute("poster"));
        }
        for(let i = 0; i < audios.length; i++){
            fileArr.push(audios[i].getAttribute("src"));
        }
        api.deleteFiles(fileArr);
    }

    // 点击删除的回调
    delete = ()=>{
        var blog = this.props.blog;
        api.getMySimpleInfo()
        .then(res=>{
            if(res.code === 200) {
                if(blog.authorId === res.data.userId){
                    this.setState({deleteModal: {visible: true, confirmLoading: false}});
                } else {
                    message.error("不能删除其他用户的博客");
                }
            }
        })
        .catch(error=>{message.error(error)});
    }

    // 确认删除的回调
    deleteOk = ()=>{
        this.setState({deleteModal: {visible: true, confirmLoading: true}});
        var blogId = this.props.blog.blogId;
        api.deleteBlog(blogId)
        .then(res=>{
            this.setState({deleteModal: {visible: false, confirmLoading: false}});
            if(res.code === 200) {
                message.success("删除成功");
                this.deleteFilesArr(this.props.blog.content);
                setTimeout(() => { // 请求成功后
                    window.history.go(-1);
                }, 500);
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{
            this.setState({deleteModal: {visible: false, confirmLoading: false}});
            message.error(error);
        })
    }

    // 取消删除
    deleteCancel = ()=>{
        this.setState({deleteModal: {visible: false, confirmLoading: false}});
    }

    render() {
        return (
        <>
            <Tooltip title="喜欢">
                <span onClick={this.like}>
                    {React.createElement(this.state.like.action === 'liked' ? LikeFilled : LikeOutlined)}
                    <span className={BlogReadBar.comment_action}>{this.state.like.likes}</span>
                </span>
            </Tooltip>
            
            <Tooltip title="不喜欢" className={BlogReadBar.bar}>
                <span onClick={this.dislike}>
                    {React.createElement(this.state.like.action === 'disliked' ? DislikeFilled : DislikeOutlined)}
                    <span className={BlogReadBar.comment_action}>{this.state.like.dislikes}</span>
                </span>
            </Tooltip>

            <Tooltip title="收藏" className={BlogReadBar.bar}>
                <span onClick={this.collection}>
                    {React.createElement(this.state.collection.isCollection ? StarFilled : StarOutlined)}
                    <span className={BlogReadBar.comment_action}>{this.state.collection.collectionNum}</span>
                </span>
            </Tooltip>

            <Tooltip title="修改" className={[BlogReadBar.bar,this.props.userinfo.userId === this.props.blog.authorId ? BlogReadBar.show : BlogReadBar.unshow]}>
                <EditOutlined onClick={this.edit}/>
            </Tooltip>
            
            <Tooltip title="删除" className={[BlogReadBar.bar,this.props.userinfo.userId === this.props.blog.authorId ? BlogReadBar.show : BlogReadBar.unshow]}>
                <DeleteOutlined onClick={this.delete}/>
            </Tooltip>

            {/* 点击删除出现的弹窗 */}
            <Modal
                title="确认删除"
                visible={this.state.deleteModal.visible}
                onOk={this.deleteOk}
                confirmLoading={this.state.deleteModal.confirmLoading}
                onCancel={this.deleteCancel}
                okText="确认"
                cancelText="取消"
            >
                <p>{this.state.deleteModal.confirmLoading ? '请稍等' : '删除后无法恢复，请确认是否需要进行删除操作'}</p>
            </Modal>

            {/* 前往登录的弹窗 */}
            <Modal 
                title="未登录" 
                visible={this.state.tokenModal} 
                onOk={this.toLogin} 
                onCancel={this.cancelLogin}
                okText="确认"
                cancelText="取消"
            >
                <p>您尚未登录，是否前往登录页面？</p>
            </Modal>  
        </>
        )
    }

    componentDidMount(){
        this.getData();
    }
}
