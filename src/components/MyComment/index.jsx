import MyComment from './index.module.css';
import React, { Component } from 'react';
import { Avatar, Tooltip, Modal, Comment, List, Form, Button, Input, message, Popconfirm } from 'antd';
import { DeleteOutlined, LoadingOutlined } from '@ant-design/icons';
import moment from 'moment';
import momentLocale from 'moment/locale/zh-cn';
import api from '../../request/api';

const { TextArea } = Input;

const Editor = ({ onChange, onSubmit, submitting, value }) => (
    <>
        <Form.Item>
            <TextArea rows={4} onChange={onChange} value={value} />
        </Form.Item>
        <Form.Item>
            <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
                回复
            </Button>
        </Form.Item>
    </>
);

export default class index extends Component {

    state = {
        commentOfCommentModal: {
            value: '', 
            visible: false, 
            confirmLoading: false, 
            to: {
                id: '', 
                authorId: -1, 
                name: '', 
                avatar: '', 
                content: ''
            }
        }
        , commentEditor: {confirmLoading: false, value: ''}
        , commentData: [
            {
                id: '1',
                authorId: -1,
                author: 'Han Solo',
                avatar: 'https://joeschmoe.io/api/v1/random',
                content: '测试',
                datetime: '2022-2-9 13:50:00',
                commentOf: null,
            },
            {
                id: '2',
                authorId: -1,
                author: 'Han Solo',
                avatar: 'https://joeschmoe.io/api/v1/random',
                content: '测试',
                datetime: '2022-2-8 13:50:00',
                commentOf: null,
            },
            {
                id: '3',
                authorId: -1,
                author: 'Han Solo',
                avatar: 'https://joeschmoe.io/api/v1/random',
                content:  '测试',
                datetime: '2022-2-7 13:50:00',
                commentOf: {
                    id: '1',
                    authorId: -1,
                    author: 'Han Solo',
                    avatar: 'https://joeschmoe.io/api/v1/random',
                    content: '测试'
                }
            },
        ]
        , tokenModal: false
        , isDeleteCommenting: false
    }

    getData = ()=>{
        api.getCommentListForBlog(this.props.blog.blogId)
        .then(res=>{
            if(res.code === 200) {
                var commentData = res.data;
                this.setState({commentData});
                // 获取数据成功后进行一次滚动操作，以便用户定位回复按钮
                this.scrollToComBtn();
            } else {
                console.log(res.msg);
            }
        })
        .catch(error=>{console.log(error)})
    }
    
    // 评论框的 onChange
    onCommentChange = (e)=>{
        const {commentEditor} = this.state;
        this.setState({commentEditor: {...commentEditor, value: e.target.value}});
    }

    // 提交评论
    submitComment = ()=>{
        const {commentData, commentEditor} = this.state;
        const {userinfo} = this.props;
        if(commentEditor.value === null || commentEditor.value === ''){
            return;
        }
        this.setState({commentEditor: {confirmLoading: true, value: commentEditor.value}});
        api.postComment(this.props.blog.blogId, commentEditor.value)
        .then(res=>{
            this.setState({commentEditor: {confirmLoading: false, value: ''}});
            if(res.code === 200) {
                this.setState({
                    commentData: [
                        ...commentData,
                        {
                            id: res.data.id,
                            authorId: userinfo.userId,
                            author: userinfo.userName,
                            avatar: userinfo.avatar,
                            content: commentEditor.value,
                            datetime: res.data.datetime,
                            commentOf: null,
                        },
                    ]
                });
            } else {
                if (res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{
            this.setState({commentEditor: {confirmLoading: false, value: ''}});
            message.error(error)
        });
    }

    // 点击回复的回调
    comOfCom = (item)=>{
        this.setState({commentOfCommentModal: {
            value: '', 
            visible: true, 
            confirmLoading: false, 
            to: {
                id: item.id, 
                authorId: item.authorId,
                name: item.author, 
                avatar: item.avatar, 
                content: item.content
            }
        }});
    }

    // 评论的回复编辑框的 onchange
    comOfComOnChange = (e)=>{
        const {commentOfCommentModal} = this.state;
        this.setState({commentOfCommentModal: {value: e.target.value, visible: commentOfCommentModal.visible, confirmLoading: commentOfCommentModal.confirmLoading, to: commentOfCommentModal.to}});
    }

    // 给评论的回复
    comOfComOk = ()=>{
        const {commentData, commentOfCommentModal} = this.state;
        const {userinfo} = this.props;
        if(commentOfCommentModal.value === null || commentOfCommentModal.value === ''){
            return;
        }
        this.setState({commentOfCommentModal: {value: commentOfCommentModal.value, visible: commentOfCommentModal.visible, confirmLoading: true, to: commentOfCommentModal.to}});
        api.postCommentOfComment(this.props.blog.blogId, commentOfCommentModal.value, commentOfCommentModal.to.id)
        .then(res=>{
            this.setState({commentOfCommentModal: {value: '', visible: false, confirmLoading: false, to: {name: '', avatar: '', content: ''}}});
            if(res.code === 200){
                this.setState({
                    commentData: [
                        ...commentData,
                        {
                            id: res.data.id,
                            author: userinfo.userName,
                            avatar: userinfo.avatar,
                            content: commentOfCommentModal.value,
                            datetime: res.data.datetime,
                            commentOf: {
                                id: commentOfCommentModal.to.id,
                                authorId: commentOfCommentModal.to.authorId,
                                author: commentOfCommentModal.to.name,
                                avatar: commentOfCommentModal.to.avatar,
                                content: commentOfCommentModal.to.content,
                            },
                        },
                    ]                    
                });
            } else {
                if (res.code === 401) {
                    this.setState({tokenModal: true});
                } else {
                    message.error(res.msg);
                }
            }
        })
        .catch(error=>{
            this.setState({commentOfCommentModal: {value: '', visible: false, confirmLoading: false, to: {name: '', avatar: '', content: ''}}});
            message.error(error);
        });
    }

    // 取消评论的回复
    comOfComCancel = ()=>{
        const {commentOfCommentModal} = this.state;
        this.setState({commentOfCommentModal: {value: commentOfCommentModal.value, visible: false, confirmLoading: false, to: commentOfCommentModal.to}});
    }

    // 删除评论
    deleteComment = (commentId)=>{
        var commentData = this.state.commentData;
        this.setState({isDeleteCommenting: true});
        api.deleteComment(commentId)
        .then(res=>{
            this.setState({isDeleteCommenting: false});
            if(res.code === 200) {
                message.success("删除成功");
                // 删除本地数据中的评论信息
                commentData.forEach(function(item, index, arr) {
                    if(item.id === commentId) {
                        arr.splice(index, 1);
                    }
                });
                this.setState({commentData});
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{
            this.setState({isDeleteCommenting: false});
            message.error(error)
        });
    }

    // 滚向回复按钮的操作
    scrollToComBtn = ()=>{
        var hash = window.decodeURIComponent(window.location.hash);
        var hashPart = hash.split('#')[1];
        var comBtn = document.getElementById(hashPart)
        if(comBtn){
            comBtn.scrollIntoView();
        }
        api.knowReply(hashPart);
    }

    // 前往登录
    toLogin = ()=>{
        this.setState({tokenModal: false});
        window.location.href = "/identity"
    }

    // 取消登录
    cancelLogin = ()=>{
        this.setState({tokenModal: false});
    }

    componentDidMount(){
        // 获取评论数据
        this.getData();
    }

    render() {
        moment.updateLocale('zh-cn', momentLocale);
        return (
        <>
            {/* 评论编辑框 */}
            <Comment
            author={this.props.userinfo.userName}
            avatar={<Avatar src={this.props.userinfo.avatar} alt={this.props.userinfo.userName} />}
            content={
                <Editor
                onChange={this.onCommentChange}
                onSubmit={this.submitComment}
                submitting={this.state.commentEditor.confirmLoading}
                value={this.state.commentEditor.value}
                />
            }
            />

            {/* 评论列表 */}
            <List
            className="comment-list"
            header={`共 ${this.state.commentData.length} 评论`}
            itemLayout="horizontal"
            dataSource={this.state.commentData}
            renderItem={item => (
                <li>
                    <Comment
                    actions={item.authorId === this.props.userinfo.userId ? [
                        (<span id={item.id} key="comment-list-reply-to-0" onClick={()=>this.comOfCom(item)}>回复</span>),
                        (
                            <Popconfirm
                            title="是否确定删除这条评论？"
                            onConfirm={()=>{this.deleteComment(item.id)}}
                            onCancel={()=>{}}
                            okText="确定"
                            cancelText="取消"
                          >
                            {this.state.isDeleteCommenting ?  <LoadingOutlined /> : <DeleteOutlined />}
                          </Popconfirm>
                        )
                    ] : 
                    [<span id={item.id} key="comment-list-reply-to-0" onClick={()=>this.comOfCom(item)}>回复</span>]}
                    key={item.id}
                    author={(
                        <p onClick={()=>{window.location.href = `/authorpage/${item.authorId}`}}>
                            {item.author}
                        </p>
                    )}
                    avatar={
                        <Avatar onClick={()=>{window.location.href = `/authorpage/${item.authorId}`}} src={item.avatar} alt={item.author} />
                    }
                    content={(
                        <p>{item.content}</p>
                    )}
                    datetime={(
                        <Tooltip title={moment(item.datetime).format('YYYY-MM-DD HH:mm:ss')}>
                            <span>{moment(item.datetime).fromNow()}</span>
                        </Tooltip>
                    )}
                    >
                        {
                            item.commentOf === null ? null :
                            <Comment className={MyComment.commentOfComment}
                            key={item.commentOf.id}
                            author={(
                                <p onClick={()=>{window.location.href = `/authorpage/${item.commentOf.authorId}`}}>
                                    {item.commentOf.author}
                                </p>
                            )}
                            avatar={
                                <Avatar 
                                onClick={()=>{window.location.href = `/authorpage/${item.commentOf.authorId}`}} 
                                src={item.commentOf.avatar} 
                                alt={item.author} />
                            }
                            content={(
                                <p>{item.commentOf.content}</p>
                            )}
                            />
                        }
                    </Comment>
                </li>
                )}
            />

            {/* 给品论的回复弹窗 */}
            <Modal
                title={`回复：${this.state.commentOfCommentModal.to.name}`}
                visible={this.state.commentOfCommentModal.visible}
                onOk={this.comOfComOk}
                confirmLoading={this.state.commentOfCommentModal.confirmLoading}
                onCancel={this.comOfComCancel}
                okText="回复"
                cancelText="取消"
            >
                {this.state.commentOfCommentModal.confirmLoading ? '请稍等' : 
                    <Form.Item>
                        <TextArea rows={3} onChange={this.comOfComOnChange} value={this.state.commentOfCommentModal.value} />
                    </Form.Item>
                }
            </Modal> 
            
            {/* 前往登录的弹窗 */}
            <Modal 
                title="未登录" 
                visible={this.state.tokenModal} 
                onOk={this.toLogin} 
                onCancel={this.cancelLogin}
                okText="确认"
                cancelText="取消"
            >
                <p>您尚未登录，是否前往登录页面？</p>
            </Modal>                                    
        </>
        )
    }
}
