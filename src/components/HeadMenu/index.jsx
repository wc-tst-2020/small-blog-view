import HeadMenu from './index.module.css';
import React, { Component } from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { Menu, Avatar, Input, Badge } from 'antd';
import { AppstoreOutlined, HomeOutlined, EditOutlined  } from '@ant-design/icons';

export default class index extends Component {

    state = {current: "main"};

    handleClick = (e) => {
        this.props.handleClick(e);
        this.setState({current: e.key});
    };

    render() {
        return (
            <Fragment>
                <img className={HeadMenu.logo} src='/images/small blog.png' alt='small blog'/>
                <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal" theme={this.props.theme}>
                    <Menu.Item key="main" icon={<HomeOutlined />}>
                        首页
                    </Menu.Item>
                    
                    <Menu.Item key="recommend" icon={<AppstoreOutlined />}>
                        推荐
                    </Menu.Item>
            
                    <Menu.Item key="search" disabled style={{ marginLeft: 'auto' }}>
                        <Input.Search className={HeadMenu.topSearch} placeholder="请输入关键词" onSearch={this.props.onSearch} enterButton />
                    </Menu.Item>
            
                    <Menu.Item key="write" icon={<EditOutlined />} >
                        写博客
                    </Menu.Item>
            
                    <Menu.Item key="avatar" className='avatar'>
                        <Badge size="small" count={this.props.msgCount}>
                            <Avatar size="large" src={this.props.avatar}/>
                        </Badge>
                    </Menu.Item>
                </Menu>
            </Fragment>
        );
    }

    componentDidMount(){
        var current = "main";
        switch(this.props.current){
            case "recomblog" :
                current = "recommend";
                break;
            case "write" :
                current = "write";
                break;
            default:
                current = "main";

        }
        this.setState({current})
    }
}
