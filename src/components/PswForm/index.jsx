import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd';
import api from '../../request/api'

export default class index extends Component {

    state = {
        changing: false
    }

    onFinish = (values) => {
        this.setState({changing: true});
        api.changePassword(values.oldPassword, values.password)
            .then(res=>{
                this.setState({changing: false});
                if(res.code === 200){
                    message.success('修改成功',1);
                    setTimeout(()=>{
                        this.props.cancel();
                    },1000);
                } else {
                    message.error(res.msg);
                }
            })
            .catch(error=>{message.error(error)})
    };

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    formLayout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 20,
        },
    }

    render() {
        return (
        <>
            <Form
                name="basic"
                {...this.formLayout}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="旧密码"
                    name="oldPassword"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入密码！',
                        }
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="新密码"
                    name="password"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入密码！',
                        },
                        ()=>({
                            validator(_, value){
                                if(!value){ // 如果没有输入value则直接放行
                                    return Promise.resolve();
                                }
                                if(value.length >= 4 && value.length <= 255){
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('密码的长度必须在4~255之内!'));
                            }
                        })
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="确认新密码"
                    name="confirmPassword"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请确认密码！',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                    
                                return Promise.reject(new Error('您输入的两个密码不匹配！'));
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>
                
                <Form.Item
                    wrapperCol={{
                        offset: this.formLayout.labelCol.span,
                        span: this.formLayout.wrapperCol.span,
                    }}
                >
                    <Button type="primary" htmlType="submit" loading={this.state.changing}>
                    确定修改
                    </Button>
                </Form.Item>
            </Form>        
        </>
        )
    }
}
