import React, { Component } from 'react'
import { Form, Input, Button, message } from 'antd';
import api from '../../request/api'

export default class index extends Component {

    state = {
        signUping: false
    }

    onFinish = (values) => {
        this.setState({signUping: true});
        api.signUp(values.email,values.username,values.password)
            .then(res=>{
                console.log(res)
                this.setState({signUping: false});
                if(res.code === 200){
                    message.success('注册成功，即将切换到登录页面',1);
                    setTimeout(()=>{
                        this.props.onClick();
                    },1000);
                } else {
                    message.error(res.msg);
                }
            })
            .catch(error=>{
                this.setState({signUping: false});
                message.error(error)
            })
    };

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    formLayout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 20,
        },
    }

    render() {
        return (
        <>
            <Form
                name="basic"
                {...this.formLayout}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                autoComplete="off"
            >
                 <Form.Item
                    label="邮箱"
                    name="email"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入邮箱！',
                        },
                        {
                            type: 'email',
                            message: '请输入有效的邮箱地址！',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="用户名"
                    name="username"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入用户名！',
                        },
                        ()=>({
                            validator(_, value){
                                if(!value){ // 如果没有输入value则直接放行
                                    return Promise.resolve();
                                }
                                if(value.length >= 1 && value.length <= 100){
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('用户名的长度必须在1~100之内!'));
                            }
                        })
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="密码"
                    name="password"
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请输入密码！',
                        },
                        ()=>({
                            validator(_, value){
                                if(!value){ // 如果没有输入value则直接放行
                                    return Promise.resolve();
                                }
                                if(value.length >= 4 && value.length <= 255){
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('密码的长度必须在4~255之内!'));
                            }
                        })
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="确认密码"
                    name="confirmPassword"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请确认密码！',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                    
                                return Promise.reject(new Error('您输入的两个密码不匹配！'));
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="已有账户？"
                    name="login"
                >
                    <a {...this.props}>返回登录</a>
                </Form.Item>
                
                <Form.Item
                    wrapperCol={{
                        offset: this.formLayout.labelCol.span,
                        span: this.formLayout.wrapperCol.span,
                    }}
                >
                    <Button type="primary" htmlType="submit" loading={this.state.signUping}>
                    注册
                    </Button>
                </Form.Item>
            </Form>        
        </>
        )
    }
}
