import './App.css';
import React, { Component } from 'react'
import Blog from './pages/Blog'
import UserPage from './pages/UserPage'
import AuthorPage from './pages/AuthorPage'
import Identity from './pages/Identity'
import UserInfo from './pages/UserInfo'
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/blog' component={Blog}/>
          <Route path='/userpage' component={UserPage}/>
          <Route path='/authorpage/:authorid' component={AuthorPage}/>
          <Route path='/identity' component={Identity}/>
          <Route path='/userinfo' component={UserInfo}/>
          <Redirect to='/blog'/>
        </Switch>
      </BrowserRouter>
    )
  }
}
