var ip_address = '10.30.23.124'

var constant = {
    base_url: "http://"+ip_address+":8086",
    base_websock_url: "ws://"+ip_address+":8086"
}

export default constant;