import request from "./request";

const api = {
  // 获取用户信息，如果已登录，则会返回简单的用户信息
  getMySimpleInfo(){
    return request({
      url: "/my-simple-info",
      method: "post"
    });
  },

  // 获取用户全部信息
  getMyFullInfo(){
    return request({
      url: "/my-full-info",
      method: "post"
    });
  },

  // 获取用户的信息
  getUserSimpleInfo(userId){
    return request({
      url: "/simple-userinfo",
      method: "post",
      data: {
        userId
      }
    });
  },

  // 登录
  login(email,password){
    return request({
      url: "/login",
      method: "post",
      data: {email,password}
    });
  },

  // 退出登录
  logout(){
    return request({
      url: "/logout",
      method: "post"
    });
  },

  // 注册
  signUp(email,userName,password){
    return request({
      url: "/signup",
      method: "post",
      data: {
        email,
        userName,
        password
      }
    });
  },

  // 获取作者信息
  getSimpleUserInfo(userId){
    return request({
      url: "/simple-userinfo",
      method: "post",
      data: {
        userId
      }
    });
  },

  // 改名
  changeUserName(newUserName){
    return request({
      url: "/change-username",
      method: "post",
      data: {
        newUserName
      }
    });
  },

  // 修改密码
  changePassword(oldPassword, newPassword){
    return request({
      url: "/change-password",
      method: "post",
      data: {
        oldPassword,
        newPassword
      }
    });
  },

  // 发布博客
  postBlog(title, content){
    return request({
      url: "/blog/post",
      method: "post",
      data: {
        title,
        content
      }
    });
  },

  // 获取博客信息
  readBlog(blogId){
    return request({
      url: "/blog/read",
      method: "post",
      data: {
        blogId
      }
    });
  },

  // 获取博客信息
  updateBlog(blogId,title,content){
    return request({
      url: "/blog/update",
      method: "post",
      data: {
        blogId,
        title,
        content
      }
    });
  },

  // 删除博客
  deleteBlog(blogId){
    return request({
      url: "/blog/delete",
      method: "post",
      data: {
        blogId
      }
    });
  },  

  // 喜欢
  agreeBlog(blogId){
    return request({
      url: "blog/like",
      method: "post",
      data: {
        blogId
      }
    });    
  },

  // 不喜欢
  disagreeBlog(blogId){
    return request({
      url: "blog/dislike",
      method: "post",
      data: {
        blogId
      }
    });  
  },

  // 点赞相关数据
  getLikesOfBlog(blogId){
    return request({
      url: "blog/likes-info",
      method: "post",
      data: {
        blogId
      }
    });  
  },

  // 收藏博客
  collectBlog(blogId){
    return request({
      url: "blog/collect",
      method: "post",
      data: {
        blogId
      }
    });  
  }, 
  
  // 取消收藏博客
  disCollectBlog(blogId){
    return request({
      url: "blog/cancel-collect",
      method: "post",
      data: {
        blogId
      }
    });  
  },  
  
  // 获取博客收藏信息
  getCollectionInfo(blogId){
    return request({
      url: "blog/collect-info",
      method: "post",
      data: {
        blogId
      }
    });  
  }, 

  // 获取所有博客列表
  getMainBlogList(curPage, pageSize){
    return request({
      url: "/blog-list/main",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });  
  },  
  
  // 获取我的博客列表
  getMyBlogList(curPage, pageSize){
    return request({
      url: "/blog-list/mine",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });  
  },
  
  // 获取作者的博客列表
  getBlogListByAuthor(userId, curPage, pageSize){
    return request({
      url: "/blog-list/by-author",
      method: "post",
      data: {
        userId,
        curPage,
        pageSize
      }
    });  
  },  

  // 获取收藏博客列表
  getCollectionBlogList(curPage, pageSize){
    return request({
      url: "/blog-list/collection",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });  
  },   
  
  // 获取搜索博客列表
  getBlogListBySearch(keyword, curPage, pageSize){
    return request({
      url: "/blog-list/search",
      method: "post",
      data: {
        keyword,
        curPage,
        pageSize
      }
    });  
  }, 
  
  // 发表评论
  postComment(blogId, content){
    return request({
      url: "/comment/post",
      method: "post",
      data: {
        blogId,
        content
      }
    });  
  },   

  // 发表回复
  postCommentOfComment(blogId, content, commentOf){
    return request({
      url: "/comment/post-reply",
      method: "post",
      data: {
        blogId,
        content,
        commentOf
      }
    });  
  },  
  
  // 博客的评论列表
  getCommentListForBlog(blogId){
    return request({
      url: "/comment/blog-comment-list",
      method: "post",
      data: {
        blogId
      }
    });  
  }, 

  // 用户的被回复列表
  getCommentListForMe(curPage, pageSize){
    return request({
      url: "/comment/my-reply-list",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });  
  }, 

  // 让回复的状态变为已知
  knowReply(commentId){
    return request({
      url: "/comment/know-reply",
      method: "post",
      data: {
        commentId
      }
    });  
  },

  // 删除评论
  deleteComment(commentId){
    return request({
      url: "/comment/delete",
      method: "post",
      data: {
        commentId
      }
    });  
  }, 

  // 获取两位用户之间的聊天记录
  getChatList(fromUserId){
    return request({
      url: "/chat/list",
      method: "post",
      data: {
        userId: fromUserId
      }
    });    
  },
 
  // 获取用户自身收到哪些用户发送过来的私信的分组列表
  getChatGroup(curPage, pageSize){
    return request({
      url: "/chat/group-list",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });
  },

  // 获取未读消息数
  getNewMsgCount(){
    return request({
      url: "/message/count",
      method: "post"
    });
  },

  // 获取未读消息数
  recordReadBlog(blogId){
    return request({
      url: "/recommend/read",
      method: "post",
      data: {
        blogId
      }
    });
  },  

  // 获取推荐博客列表
  getRecommendBlog(curPage, pageSize){
    return request({
      url: "/recommend/blog",
      method: "post",
      data: {
        curPage,
        pageSize
      }
    });
  },    

  // 获取推荐用户列表
  getRecommendUser(){
    return request({
      url: "/recommend/user",
      method: "post"
    });
  },  

  // 删除多余文件
  deleteFiles(fileNames){
    return request({
      url: "/upload-delete",
      method: "post",
      data: {
        list: fileNames
      }
    });    
  }
}

export default api;
