import axios from "axios";
import constant from './constant';

// 创建axios 实例
const service = axios.create({
  baseURL:
    process.env.NODE_ENV === "production"
      ? process.env.VUE_APP_REQUEST_URL
      : constant.base_url, // api的base_url
  timeout: 10000 // 请求超时时间
});

// request 拦截器
service.interceptors.request.use(
  config => {
    // config.headers.Authorization = getCookie("Authorization");
    // 这里可以自定义一些config 配置
    // 判断是否存在token，如果存在的话，则每个http header都加上token
    //let token = sessionStorage.getItem('Authorization')
    let token = localStorage.getItem('Authorization')
    if (!config.headers.hasOwnProperty('Authorization') && token) {
        config.headers.Authorization = token;
    }
    return config;
  },
  error => {
    //  这里处理一些请求出错的情况
    Promise.reject(error);
  }
);

// response 拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    // 这里处理一些response 正常放回时的逻辑
    return res;
  },
  error => {
    // 这里处理一些response 出错时的逻辑
    return Promise.reject(error);
  }
);

export default service;
