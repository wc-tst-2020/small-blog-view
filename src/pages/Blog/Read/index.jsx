import Read from './index.module.css';
import React, { Component } from 'react';
import { Row, Col, Avatar, Typography } from 'antd';
import { Fragment } from 'react/cjs/react.production.min';
import BlogReadBar from '../../../components/BlogReadBar';
import MyComment from '../../../components/MyComment';
import api from '../../../request/api'
import moment from 'moment';

const { Title, Paragraph, Text } = Typography;

export default class index extends Component {

    state = { 
        blog: {
            blogId:  1
            , title: '测试'
            , content: '<p>MySQL 使用联合索引时，需要满足最左前缀原则。下面举例对其进行说明：<br><br><pre><code class="lang-java">    1. 一个 2 列的索引 (name, age)，对 (name)、(name, age) 上建立了索引；2. 一个 3 列的索引 (name, age, sex)，对 (name)、(name, age)、(name, age, sex) 上建立了索引。</code></pre><p><br><br>1、 B+ 树的数据项是复合的数据结构，比如：(name, age, sex) 的时候，B+ 树是按照从左到右的顺序来建立搜索树的，比如：当(小明, 22, 男)这样的数据来检索的时候，B+ 树会优先比较 name 来确定下一步的所搜方向，如果 name 相同再依次比较 age 和 sex，最后得到检索的数据。但当 (22, 男) 这样没有 name 的数据来的时候，B+ 树就不知道第一步该查哪个节点，因为建立搜索树的时候 name 就是第一个比较因此，必须要先根据 name 来搜索才能知道下一步去哪里查询。<br><br><br>2、 当 (小明, 男) 这样的数据来检索时，B+ 树可以用 name 来指定搜索方向，但下一个字段 age 的缺失，所以只能把名字等于小明的数据都找到，然后再匹配性别是男的数据了， 这个是非常重要的性质，即索引的最左匹配特性。<br><br><br><strong>关于最左前缀的补充：</strong><br>1、最左前缀匹配原则会一直向右匹配直到遇到范围查询（&gt;、&lt;、between、like）就停止匹配，比如：a = 1 and b = 2 and c &gt; 3 and d = 4 如果建立 (a, b, c, d) 顺序的索引，d 是用不到索引的。如果建立 (a, b, d, c) 的索引则都可以用到，a、b、d 的顺序可以任意调整。<br>2、= 和 in 可以乱序，比如：a = 1 and b = 2 and c = 3 建立 (a, b ,c) 索引可以任意顺序，MySQL 的优化器会优化成索引可以识别的形式。<br><br><br><br><br>原文来自：<a href="https://www.iamshuaidi.com/1439.html">https://www.iamshuaidi.com/1439.html</a></p>'
            , authorId: '1'
            , authorAvatar: "https://joeschmoe.io/api/v1/random"
            , authorName: "测试"
            , postTime: "2022-1-29 12:13:00"
            , lastUpdate: "2022-1-29 12:13:00"
        } 
        , userinfo: {userId:'-1', avatar: 'https://joeschmoe.io/api/v1/random', userName: '未登录'}
        ,sideWidth: 1
        ,contentWidth: 22
    };

    getData = ()=>{
        // 获取用户信息
        api.getMySimpleInfo()
        .then(res=>{
            if(res.code === 200) {
                var userinfo = {
                    userId: res.data.userId,
                    avatar: res.data.avatar,
                    userName: res.data.userName
                }
                this.setState({userinfo});
            } else {
                console.log(res.msg)
            }
        })
        .catch(error=>{console.log(error)})

        var blogId = this.props.match.params.blogid;
        // 获取博客信息
        api.readBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                var blog = {
                    blogId: res.data.blogId,
                    title: res.data.title,
                    content: res.data.content,
                    authorId: res.data.authorId,
                    authorAvatar: res.data.authorAvatar,
                    authorName: res.data.authorName,
                    postTime: res.data.postTime,
                    lastUpdate: res.data.lastUpdate
                }
                this.setState({blog});
                api.recordReadBlog(blogId);
            }
        })
        .catch(error=>{console.log(error)})
    }

    toAuthorPage = ()=>{
        this.props.history.push(`/authorpage/${this.state.blog.authorId}`);
    }

    render() {
        return (
            <Fragment>
                <Row>
                    <Col span={this.state.sideWidth}/>
                    <Col span={this.state.contentWidth} className={Read.blog}>
                        <Typography>
                            {/* 标题 */}
                            <Title className={Read.title}>{this.state.blog.title}</Title>

                            {/* 作者信息 */}
                            <Row onClick={()=>{this.toAuthorPage()}}>
                                <Col span={4}/>
                                <Col span={16} className={Read.author}>
                                    <Avatar src={this.state.blog.authorAvatar}/>&nbsp;&nbsp;&nbsp;<div className={Read.authorName}>{this.state.blog.authorName}</div>
                                </Col>
                                <Col span={4}/>
                            </Row>
                            
                            {/* 正文 */}
                            <Paragraph>
                                <div className={Read.media} dangerouslySetInnerHTML={{ __html: this.state.blog.content}}></div>
                            </Paragraph>

                            {/* 写作时间 */}
                            <br/><br/>
                            <Row>
                                <Col span={24}><Text disabled>本文提交于 { moment(this.state.blog.postTime).format('YYYY-MM-DD HH:mm:ss')}，最近修改于 {moment(this.state.blog.lastUpdate).format('YYYY-MM-DD HH:mm:ss')}</Text></Col>
                            </Row>
                            <br/>

                            {/* 工具按钮栏 */}
                            <BlogReadBar key={Math.random()} userinfo={this.state.userinfo} blog={this.state.blog}/>
                        </Typography>

                        {/* 评论区 */}
                        <br/>
                        <MyComment key={Math.random()} userinfo={this.state.userinfo} blog={this.state.blog}/>
                    </Col>
                    <Col span={this.state.sideWidth}/>
                </Row>
            </Fragment>
        );
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount(){
        this.getData();
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
    }
}