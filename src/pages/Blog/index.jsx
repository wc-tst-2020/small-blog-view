import Blog from './index.module.css';
import React, { Component } from 'react';
import { Layout, Affix, Modal, message } from 'antd';
import HeadMenu from '../../components/HeadMenu';
import Main from './Main';
import Read from './Read';
import Write from './Write'
import RecomBlog from './RecomBlog'
import SearchBlog from './SearchBlog'
import EditBlog from './EditBlog'
import {Route, Switch, Redirect} from 'react-router-dom'
import api from '../../request/api';

const {Header, Content, Footer} = Layout;

export default class index extends Component {

    state = {
        userInfo: {userId: -1, msgCount: 0, avatar: "https://joeschmoe.io/api/v1/random"},
        userInfoModal: false,
    };

    handleClick = (e) => {
        //console.log(e);
        switch(e.key){
            case 'main': 
                this.props.history.push('/blog/main/1') 
                break
            case 'recommend': 
                this.props.history.push('/blog/recomblog/1') 
                break
            case 'write': 
                api.getMySimpleInfo()
                .then(res=>{
                    if(res.code === 200) {
                        var userInfo = {
                            userId: res.data.userId,
                            msgCount: res.data.msgCount,
                            avatar: res.data.avatar
                        }
                        this.setState({userInfo});
                        this.props.history.push('/blog/write')  
                    } else {
                        this.setState({userInfoModal: true});
                    }
                })
                .catch(error=>{message.error(error)})
                break
            case 'avatar': 
                api.getMySimpleInfo()
                    .then(res=>{
                        if(res.code === 200) {
                            var userInfo = {
                                userId: res.data.userId,
                                msgCount: res.data.msgCount,
                                avatar: res.data.avatar
                            }
                            this.setState({userInfo});
                            const w = window.open('about:blank')
                            w.location.href = '/userpage'   
                        } else {
                            this.setState({userInfoModal: true});
                        }
                    })
                    .catch(error=>{message.error(error)})
                break
            default:
                break             
        }
    }

    onSearch = (value) => {
        //console.log(value);
        if(!value){
            return;
        }
        var key = value.trim();
        if(key.length <= 0){
            return;
        }
        this.props.history.push(`/blog/searchblog/${key}/1`);
    }

    toLogin = ()=>{
        this.setState({tokenModal: false});
        this.props.history.push('/identity');
    }

    cancelLogin = ()=>{
        this.setState({userInfoModal: false});
    }
    
    getData = ()=>{
        api.getMySimpleInfo()
        .then( res =>{
            if( res.code === 200 ) {
                var userInfo = {
                    userId: res.data.userId,
                    msgCount: this.state.userInfo.msgCount,
                    avatar: res.data.avatar
                }
                this.setState({userInfo});
                // 获取用户的未读信息数
                api.getNewMsgCount().then(res=>{
                    if(res.code === 200) {
                        this.setState({userInfo: {
                            userId: userInfo.userId,
                            msgCount: res.data,
                            avatar: userInfo.avatar
                        }});
                    }
                })                
            }
        })
        .catch(error=>{})
    }

    render() {
        const {userInfoModal} = this.state;
        return (
        <>
            <Layout>
                <Affix offsetTop={0}>
                    <Header>
                        <HeadMenu theme="dark" 
                        current = {this.props.history.location.pathname.split('/')[2]}
                        handleClick = {this.handleClick}
                        onSearch={this.onSearch} 
                        msgCount={this.state.userInfo.msgCount}  
                        avatar={this.state.userInfo.avatar}/>
                    </Header>
                </Affix>
        
                <Content key={this.props.location.pathname}>
                    <Switch>
                        <Route path='/blog/main/:page' component={Main}/>
                        <Route path='/blog/read/:blogid' component={Read}/>
                        <Route path='/blog/write' component={Write}/>
                        <Route path='/blog/recomblog/:page' component={RecomBlog}/>
                        <Route path='/blog/searchblog/:key/:page' component={SearchBlog}/>
                        <Route path='/blog/editblog/:blogid' component={EditBlog}/>
                        <Redirect to='/blog/main/1'/>
                    </Switch>
                </Content>

                <Footer className={Blog.footer}></Footer>
            </Layout>
            <Modal 
                title="未登录" 
                visible={userInfoModal} 
                onOk={this.toLogin} 
                onCancel={this.cancelLogin}
                okText="确认"
                cancelText="取消"
            >
                <p>您尚未登录，是否前往登录页面？</p>
            </Modal>                       
        </>
        );
    }

    componentDidMount(){
        this.getData();
    }
}
