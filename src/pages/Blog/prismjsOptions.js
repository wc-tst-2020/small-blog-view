import 'prismjs/components/prism-java'
import 'prismjs/components/prism-php'
import 'prismjs/components/prism-c'
import 'prismjs/components/prism-cpp'

export default {
    syntaxs: [
        {
        name: 'JavaScript',
        syntax: 'javascript'
        }, {
        name: 'HTML',
        syntax: 'html'
        }, {
        name: 'CSS',
        syntax: 'css'
        }, {
        name: 'Java',
        syntax: 'java',
        }, {
        name: 'PHP',
        syntax: 'php'
        }, {
        name: 'C',
        syntax: 'c'
        }, {
        name: 'CPP',
        syntax: 'cpp'
        }
    ]
}