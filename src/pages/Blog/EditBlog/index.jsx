import EditBlog from './index.module.css'
import React, { Component } from 'react';
import { Row, Col, Input, Button, notification, message } from 'antd';
import { ReadOutlined } from '@ant-design/icons';
import BraftEditor from 'braft-editor'
import 'braft-editor/dist/index.css'
import constant from '../../../request/constant';
import api from '../../../request/api'


const openSuccessNotificationn = msg => {
    notification.success({
        message: msg,
        placement: 'bottomRight'
    });
};

const openErrorNotificationn = msg => {
    notification.error({
        message: msg,
        placement: 'bottomRight'
    });
};

const upload_url = constant.base_url + '/upload-media';

const fileSet = new Set();

const newFileSet = new Set();

const myUploadFn = (param) => {

    const serverURL = upload_url
    const xhr = new XMLHttpRequest();
    const fd = new FormData()

    const successFn = (response) => {
        const res = JSON.parse(xhr.responseText);
        if(res.code === 200){
            fileSet.add(res.data.url);
            fileSet.add(res.data.poster);
            newFileSet.add(res.data.url);
            newFileSet.add(res.data.poster);
            // 假设服务端直接返回文件上传后的地址
            // 上传成功后调用param.success并传入上传后的文件地址
            param.success({
                url: res.data.url,
                meta: {
                    id: res.data.id,
                    title: res.data.title,
                    alt: res.data.alt,
                    loop: true, // 指定音视频是否循环播放
                    autoPlay: true, // 指定音视频是否自动播放
                    controls: true, // 指定音视频是否显示控制栏
                    poster: res.data.poster, // 指定视频播放器的封面
                }
            })
        } else {
            message.error(res.msg);
            param.error({
                msg: res.msg
            })
        }
    }

    const progressFn = (event) => {
        // 上传进度发生变化时调用param.progress
        param.progress(event.loaded / event.total * 100)
    }

    const errorFn = (response) => {
        message.error("上传失败");
        // 上传发生错误时调用param.error
        param.error({
            msg: 'unable to upload.'
        })
    }

    xhr.upload.addEventListener("progress", progressFn, false)
    xhr.addEventListener("load", successFn, false)
    xhr.addEventListener("error", errorFn, false)
    xhr.addEventListener("abort", errorFn, false)

    fd.append('file', param.file)
    xhr.open('POST', serverURL, true)
    xhr.setRequestHeader('Authorization',localStorage.getItem('Authorization'));
    xhr.send(fd)
}

export default class index extends Component {

    state = {
        blogId: null,
        title: null,
        editorState: null,
        posting: false
        ,hasPost: false
        ,sideWidth: 1
        ,contentWidth: 22
    };

    // 获取文章所引用的文件
    getOldFileSet = (htmlContent)=>{
        var el = document.createElement("div");
        el.innerHTML = htmlContent;
        var imgs = el.getElementsByTagName("img");
        var videos = el.getElementsByTagName("video");
        var audios = el.getElementsByTagName("audio");
        for(let i = 0; i < imgs.length; i++){
            fileSet.add(imgs[i].getAttribute("src"));
        }
        for(let i = 0; i < videos.length; i++){
            fileSet.add(videos[i].getAttribute("src"));
            fileSet.add(videos[i].getAttribute("poster"));
        }
        for(let i = 0; i < audios.length; i++){
            fileSet.add(audios[i].getAttribute("src"));
        }
    }

    getData = ()=>{
        var blogId = this.props.match.params.blogid;
        // 获取博客信息
        api.readBlog(blogId)
        .then(res=>{
            if(res.code === 200) {
                var blog = res.data;
                // 检查是不是作者本人
                api.getMySimpleInfo()
                .then(res=>{
                    if(res.code === 200) {
                        if(res.data.userId === blog.authorId) {
                            this.setState({
                                blogId: blog.blogId,
                                title: blog.title,
                                editorState: BraftEditor.createEditorState(blog.content)
                            })
                            this.getOldFileSet(blog.content);
                        } else {
                            this.props.history.replace('/identity');
                        }
                    } else {
                        this.props.history.replace('/identity');
                    }
                })
                .catch(error=>{
                    console.log(error);
                    this.props.history.replace('/identity');
                })
            }
        })
        .catch(error=>{console.log(error)})
    }

    onTitleChange = (e)=>{
        this.setState({title: e.target.value});
    }

    // 按 ctrl + s 的回调函数
    submitContent = async () => {
        // Pressing ctrl + s when the editor has focus will execute this method
        // Before the editor content is submitted to the server, you can directly call editorState.toHTML () to get the HTML content
        //const htmlContent = this.state.editorState.toHTML()
        //const result = await saveEditorContent(htmlContent) // 保存结果
        //console.log(htmlContent);
        // openSuccessNotificationn('保存成功');
        // openErrorNotificationn('保存失败');
        console.log("功能未实现，敬请期待");
    }

    handleEditorChange = (editorState) => {
        this.setState({ editorState })
    }

    deleteFiles = (htmlContent) => {
        var el = document.createElement("div");
        el.innerHTML = htmlContent;
        var imgs = el.getElementsByTagName("img");
        var videos = el.getElementsByTagName("video");
        var audios = el.getElementsByTagName("audio");
        for(let i = 0; i < imgs.length; i++){
            fileSet.delete(imgs[i].getAttribute("src"));
        }
        for(let i = 0; i < videos.length; i++){
            fileSet.delete(videos[i].getAttribute("src"));
            fileSet.delete(videos[i].getAttribute("poster"));
        }
        for(let i = 0; i < audios.length; i++){
            fileSet.delete(audios[i].getAttribute("src"));
        }
        api.deleteFiles([...fileSet]);
    }

    postBlog = ()=>{
        this.setState({posting: true});

        if(this.state.editorState === null){
            this.setState({posting: false});
            openErrorNotificationn('内容不能为空');
            return;
        }

        const htmlContent = this.state.editorState.toHTML();
        const {title} = this.state;

        if(title === null || title === ''){
            this.setState({posting: false});
            openErrorNotificationn('标题不能为空');
            return;
        }

        if(htmlContent === null || htmlContent === '' || htmlContent === '<p></p>'){
            this.setState({posting: false});
            openErrorNotificationn('内容不能为空');
            return;
        }

        api.updateBlog(this.state.blogId,title, htmlContent)
        .then(res=>{
            this.setState({posting: false});
            if(res.code === 200){
                openSuccessNotificationn('修改成功');
                // 删除没有用上的文件
                this.deleteFiles(htmlContent);
                // 设置发布状态为已发布
                this.setState({hasPost: true});              
                // 跳转至阅读页
                setTimeout(()=>{
                    this.props.history.replace(`/blog/read/${this.state.blogId}`);
                },500);
            } else{
                openErrorNotificationn(res.msg);
            }
        })
        .catch(error=>{
            this.setState({posting: false});
            openErrorNotificationn(error);
        });
    }

    render() {
        return (
            <Row>
                <Col span={this.state.sideWidth}/>
                <Col span={this.state.contentWidth} className={EditBlog.content}>
                    <Input size="large" placeholder="请输入标题" prefix={<ReadOutlined />} value={this.state.title} onChange={this.onTitleChange}/>
                    
                    <div className={EditBlog.editor}>
                        <BraftEditor
                        media={{uploadFn: myUploadFn}}
                        value={this.state.editorState}
                        onChange={this.handleEditorChange}
                        onSave={this.submitContent}
                        />
                    </div>

                    <Row>
                        <Col span={24} className={EditBlog.post}>
                            <Button type="primary" loading={this.state.posting} onClick={()=>this.postBlog()}>确认修改</Button>
                        </Col>
                    </Row>
                </Col>
                <Col span={this.state.sideWidth}/>
            </Row>
        );
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount () {
        this.getData();
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        if(!this.state.hasPost){
            api.deleteFiles([...newFileSet]);
        }
        window.removeEventListener('resize',this.onResize);
    }
}
