import React, { Component } from 'react'
import { Modal } from 'antd'
import SignUpForm from '../../components/SignUpForm'
import LoginFrom from '../../components/LoginForm'

export default class index extends Component {

    state = {
        isLogin: true
    }

    changeForm = ()=>{
        const {isLogin} = this.state;
        this.setState({isLogin: !isLogin});
    }

    cancel = ()=>{
        this.props.history.goBack();
    }

    render() {
        return (
        <>
            <Modal
                visible={!this.state.isLogin}
                footer={null}
                onCancel={this.cancel}
                closable={false}
                width={700}
            >
                <SignUpForm onClick={this.changeForm}/>
            </Modal>

            <Modal
                visible={this.state.isLogin}
                footer={null}
                onCancel={this.cancel}
                closable={false}
                width={700}
            >
                <LoginFrom onClick={this.changeForm} goBack={this.cancel}/>
            </Modal>
        </>
        )
    }
}
