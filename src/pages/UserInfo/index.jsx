import React, { Component } from 'react'
import { Form, Input, Button, message, Avatar, Upload, Descriptions, Modal } from 'antd';
import { UploadOutlined, EditOutlined, CheckOutlined, CloseOutlined, LockOutlined } from '@ant-design/icons';
import UserInfo from './index.module.css'
import PwsForm from '../../components/PswForm'
import ImgCrop from 'antd-img-crop';
import moment from 'moment';
import api from '../../request/api'
import constant from '../../request/constant';

const upload_url = constant.base_url + '/change-avatar';

const props = {
  name: 'file',
  action: upload_url,
  headers: {
    Authorization: localStorage.getItem('Authorization'),
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      // console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} 上传成功`);
      window.location.reload();
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} 上传失败`);
    }
  },
  onPreview : async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  },
  
};

export default class index extends Component {

    state = {
      userId: -1,
      avatar: 'https://joeschmoe.io/api/v1/random',
      userName: '未登录',
      email: 'asdf@asdf.com',
      lastLogin: '2022-3-14 15:13:04',
      createDate: '2018-3-14 15:13:04',
      editting: false,
      newName: '未登录',
      newNameConfirming: false,
      pswModalVisible: false,
    }

    getData = ()=>{
      api.getMyFullInfo()
        .then(res=>{
          if(res.code === 200) {
            this.setState({
              userId: res.data.userId,
              avatar: res.data.avatar,
              userName: res.data.userName,
              email: res.data.email,
              lastLogin: res.data.lastLogin,
              createDate: res.data.createDate,
            });
          } else {
            message.error(res.msg)
            this.props.history.replace('/identity');
          }
        })
        .catch(error=>{
          message.error(error)
          this.props.history.replace('/identity');
        })
    }

    editName = ()=>{
      const {userName} = this.state;
      this.setState({editting: true, newName: userName})
    }

    editNameIng = (e)=>{
      this.setState({newName: e.target.value})
    }

    cancelEditName = ()=>{
      this.setState({editting: false});
    }

    confirmNewName = ()=>{
      const {userName, newName} = this.state;
      if(!userName || userName.length < 1 || userName.length > 100){
        message.error('用户名的长度必须在1~100之间');
      }
      if(userName === newName){
        message.error('新旧用户名一致',1);
        return;
      }
      this.setState({newNameConfirming: true})
      api.changeUserName(newName)
        .then(res=>{
          this.setState({newNameConfirming: false, editting: false})
          if(res.code === 200) {
            message.success('用户名更新成功',1);
            this.setState({userName: newName})
          } else {
            message.error(res.msg);
          }
        })
        .catch(error=>{
          this.setState({newNameConfirming: false, editting: false})
          message.error(error);
        })
    }

    callPswModal = ()=>{
      this.setState({pswModalVisible: true});
    }

    cancelPswModal = ()=>{
      this.setState({pswModalVisible: false});
    }

    render() {
        const { avatar, userName, email, lastLogin, createDate, editting, newName, newNameConfirming, pswModalVisible } = this.state;
        return (
        <>
            <Form
                name="basic"
                layout='vertical'
                autoComplete="off"
            >
                 <Form.Item className={UserInfo.center}>
                   <br/>
                  <Avatar size={128} src={avatar}/>
                </Form.Item>

                <Form.Item className={UserInfo.center}>
                  <ImgCrop rotate>
                    <Upload {...props}>
                      <Button icon={<UploadOutlined />}>上传头像</Button>
                    </Upload>
                  </ImgCrop>
                </Form.Item>
            </Form>
            <Descriptions title="个人资料" layout="vertical" className={UserInfo.userDesc}>
              <Descriptions.Item label="邮箱">{email}</Descriptions.Item>
              {
                editting ? 
                <Descriptions.Item label="用户名">
                  <Input value={newName} onChange={(e)=>{this.editNameIng(e)}}/>
                  <Button icon={<CheckOutlined />}  className={UserInfo.yesBtn} loading={newNameConfirming} onClick={this.confirmNewName}/>
                  <Button icon={<CloseOutlined />}  className={UserInfo.noBtn} onClick={this.cancelEditName}/>
                </Descriptions.Item>
                :
                <Descriptions.Item label="用户名">
                  { userName }
                  <Button icon={<EditOutlined />}  className={UserInfo.editBtn} onClick={this.editName}/>
                </Descriptions.Item>
              }
              <Descriptions.Item label="修改密码">
                <Button icon={<LockOutlined />}  onClick={this.callPswModal}>修改密码</Button>
              </Descriptions.Item> 
              <Descriptions.Item label="最近登录时间">{moment(lastLogin).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>
              <Descriptions.Item label="账号创建时间">{moment(createDate).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>
            </Descriptions>
            <Modal 
              title='修改密码'
              visible={pswModalVisible}
              onCancel={this.cancelPswModal}
              footer={null}
            >
              <PwsForm cancel={this.cancelPswModal}/>
            </Modal>
        </>
        )
    }

    componentDidMount(){
      this.getData();
    }
}
