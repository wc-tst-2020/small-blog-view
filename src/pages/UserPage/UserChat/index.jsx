import UserChat from './index.module.css';
import React, { Component } from 'react'
import { Row, Col } from 'antd';
import MyChat from '../../../components/MyChat';

export default class index extends Component {

    state = {
        sideWidth: 1
        ,contentWidth: 22
    }

    render() {
        return (
        <>
            <Row key={this.props.location.pathname}>
                <Col span={this.state.sideWidth}/>
                <Col span={this.state.contentWidth} className={UserChat.chat}>
                    <MyChat contactId={this.props.match.params.userid} height={600}/>
                </Col>
                <Col span={this.state.sideWidth}/>
            </Row>
        </>
        )
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount(){
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
    }
}
