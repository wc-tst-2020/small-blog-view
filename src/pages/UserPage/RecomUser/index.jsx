import RecomUser from './index.module.css'
import React, { Component } from 'react'
import { Row, Col, Comment, Divider, Avatar, List, message } from 'antd';
import {Link} from 'react-router-dom'
import api from '../../../request/api';

export default class index extends Component {

    state = {
        // recomUsers: [{userId: '1', userName: '测试1号', avatar: 'https://joeschmoe.io/api/v1/random"', times: 30}
        //             ,{userId: '2', userName: '测试2号', avatar: 'https://joeschmoe.io/api/v1/random"', times: 30}
        //             ,{userId: '3', userName: '测试3号', avatar: 'https://joeschmoe.io/api/v1/random"', times: 30}
        //             ,{userId: '4', userName: '测试4号', avatar: 'https://joeschmoe.io/api/v1/random"', times: 30}]
        recomUsers: []
        ,sideWidth: 1
        ,contentWidth: 22
    }

    getData = ()=>{
        api.getRecommendUser()
        .then(res=>{
            if(res.code === 200) {
                var recomUsers = res.data;
                this.setState({recomUsers});
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{
            message.error(error);
        });
    }

    render() {
        return (
        <>
            <Row key={this.props.location.pathname}>
                <Col span={this.state.sideWidth}/>
                <Col span={this.state.contentWidth} className={RecomUser.list}>
                    <List
                        itemLayout="horizontal"
                        dataSource={this.state.recomUsers}
                        renderItem={item => (
                            <li>
                                <Link to={`/authorpage/${item.userId}`}>
                                    <Comment
                                        key={item.userId}
                                        author={<span>{item.userName}</span>}
                                        avatar={<Avatar src={item.avatar} alt={item.userName} />}
                                        content={
                                            <p>您总共看过 {item.times} 次他发布的博客</p>
                                        }
                                    />
                                </Link>
                                <Divider />
                            </li>
                            )}                        
                    />
                </Col>
                <Col span={this.state.sideWidth}/>
            </Row>
        </>
        )
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount(){
        this.getData();
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
    }
}
