import PrivateMessage from './index.module.css'
import React, { Component } from 'react'
import {Row, Col, Pagination, Comment, Avatar, Badge, List, Divider, Tooltip, message } from 'antd';
import moment from 'moment';
import momentLocale from 'moment/locale/zh-cn';
import {Link} from 'react-router-dom'
import api from '../../../request/api';

export default class index extends Component {

  state = {
    // messages: [{userId: '2', userName: '测试1号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 20}
    //             ,{userId: '3', userName: '测试2号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 40}
    //             ,{userId: '4', userName: '测试6号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 30}
    //             ,{userId: '5', userName: '测试5号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 50}
    //             ,{userId: '6', userName: '测试8号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 60}
    //             ,{userId: '7', userName: '测试7号', avatar: 'https://joeschmoe.io/api/v1/random', lastMsg: '测试', lastMsgDate: '2020-3-12 15:51:00', msgCount: 0}
    //           ]
    messages: []
    , curPage: 1
    ,totalPage: 0
    ,sideWidth: 1
    ,contentWidth: 22
  }

  getData = ()=>{
    const {curPage} = this.state;
    api.getChatGroup(curPage, 10)
    .then(res=>{
      if(res.code === 200) {
        var messages = res.data.list;
        var totalPage = res.data.totalSize;
        this.setState({messages, totalPage});
      } else {
        message.error(res.msg);
      }
    })
    .catch(error=>{
      message.error(error);
    })
  }

  changePage = (curPage) => {
    // console.log(curPage);
    // this.setState({curPage});
    this.props.history.push(`/userpage/privatemessage/${curPage}`);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
      var page = parseInt(nextProps.match.params.page);
      return {curPage: page};
  }

  render() {
    moment.updateLocale('zh-cn', momentLocale);
    return (
    <>
      <Row key={this.props.location.pathname}>
        <Col span={this.state.sideWidth}/>
        <Col span={this.state.contentWidth} className={PrivateMessage.list}>
            <List
            itemLayout="horizontal"
            dataSource={this.state.messages}
            renderItem={message => (
              <li>
                  <Link to={`/userpage/userchat/${message.userId}`}>
                    <Comment
                      key={message.userId}
                      author={<span>{message.userName}</span>}
                      avatar={<Avatar src={message.avatar} alt={message.userName} />}
                      datetime={
                        <Tooltip title={moment(message.lastMsgDate).format('YYYY-MM-DD HH:mm:ss')}>
                            <span>{moment(message.lastMsgDate).fromNow()}</span>
                        </Tooltip>  
                      }
                      content={
                        <Badge count={message.msgCount}>
                            <p>{message.lastMsg}</p>
                        </Badge>
                      }
                    />
                  </Link>
                  <Divider />
              </li>
              )}
            />

            <div className={PrivateMessage.pagination}>
                <Pagination 
                showSizeChanger={false} 
                current={this.state.curPage} 
                onChange={this.changePage} 
                total={this.state.totalPage} />
            </div>
        </Col>
        <Col span={this.state.sideWidth}/>
      </Row>
    </>
    )
  }

  onResize = ()=>{
    var curWidth = document.documentElement.clientWidth;
    var sideWidth = 1;
    var contentWidth = 24 - sideWidth * 2;
    if (curWidth >= 1000) {
        sideWidth = 4;
        contentWidth = 24 - sideWidth * 2;
        this.setState({sideWidth,contentWidth});
    } else {
        if(curWidth >= 800){
            sideWidth = 2;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            sideWidth = 1;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        }
    }
  }  

  componentDidMount(){
    this.getData();
    this.onResize();
    window.addEventListener('resize',this.onResize);
  }

  componentWillUnmount(){
    window.removeEventListener('resize',this.onResize);
  }
}
