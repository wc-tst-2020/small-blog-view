import UserPage from './index.module.css';
import React, { Component } from 'react'
import { Layout, Row, Col, Avatar, Button, Menu, message, Popconfirm  } from 'antd'
import { Content, Footer } from 'antd/lib/layout/layout';
import MyBlog from './MyBlog';
import CollectionBlog from './CollectionBlog';
import PrivateMessage from './PrivateMessage';
import CommentForMe from './CommentForMe';
import RecomUser from './RecomUser';
import UserChat from './UserChat';
import {Route, Switch, Redirect, Link} from 'react-router-dom'
import api from '../../request/api'

export default class index extends Component {

  state = {
    userinfo: {userId:'-1', avatar: 'https://joeschmoe.io/api/v1/random', userName: '未登录'},
    current: 'myBlog',
  }

  getData = ()=>{
    api.getMySimpleInfo()
    .then( res =>{
        if( res.code === 200 ) {
            var userinfo = {
                userId: res.data.userId,
                msgCount: res.data.msgCount,
                avatar: res.data.avatar
            }
            this.setState({userinfo});
        } else {
          message.error(res.msg);
          this.props.history.replace('/identity');
        }
    })
    .catch(error=>{
      message.error(error);
      this.props.history.replace('/identity');
    })
  }

  changeCur = ()=>{
    var current = "myBlog";
    switch(this.props.history.location.pathname.split('/')[2]){
      case "collectionblog" : 
        current = "myCollection";
        break;
      case "privatemessage" :
        current = "message";
        break;
      case "userchat" :
        current = "message";
        break;        
      case "commentforme":
        current = "comment";
        break;
      case "recomuser" :
        current = "recom";
        break;
      default :
        current = "myBlog";
    }
    this.setState({current});
  }

  handleClick = (e)=>{
    // console.log('click ', e);
    this.setState({ current: e.key });
  }

  logout = ()=>{
    api.logout()
      .then(res=>{
        if(res.code !== 200) {
          message.error(res.msg)
        } else {
          window.close();
        }
      })
      .catch(error=>{
        message.error(error);
      })
  }

  render() {
    return (
    <>
      <Row>
        <Col span={24} className={[UserPage.center, UserPage.content]}>
          <Avatar size={128} src={this.state.userinfo.avatar}/>
        </Col>
      </Row>
      <Row>
        <Col span={24} className={[UserPage.center, UserPage.paddingBottom]}>
          <Link to='/userinfo'>
            <Button type="primary">账号信息</Button>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col span={24} className={[UserPage.center, UserPage.paddingBottom]}>
          <Popconfirm title="是否确定退出登录" onConfirm={this.logout} okText="确定" cancelText="取消">
            <Button>登出</Button>
          </Popconfirm>
        </Col>
      </Row>
      <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
        <Menu.Item key="myBlog">
          <Link to='/userpage/myblog/1'>我的</Link>
        </Menu.Item>
        <Menu.Item key="myCollection">
          <Link to='/userpage/collectionblog/1'>我的收藏</Link>
        </Menu.Item>
        <Menu.Item key="message">
          <Link to='/userpage/privatemessage/1'>私信</Link>
        </Menu.Item>
        <Menu.Item key="comment">
          <Link to='/userpage/commentforme/1'>回复</Link>
        </Menu.Item>
        <Menu.Item key="recom">
          <Link to='/userpage/recomuser'>你可能感兴趣的人</Link>
        </Menu.Item>
      </Menu>

      {/* 信息展示区 */}
      <Layout>
        <Content key={this.props.location.pathname}>
          <Switch>
            <Route path='/userpage/myblog/:page' component={MyBlog}/>
            <Route path='/userpage/collectionblog/:page' component={CollectionBlog}/>
            <Route path='/userpage/privatemessage/:page' component={PrivateMessage}/>
            <Route path='/userpage/userchat/:userid' component={UserChat}/>
            <Route path='/userpage/commentforme/:page' component={CommentForMe}/>
            <Route path='/userpage/recomuser' component={RecomUser}/>
            <Redirect to='/userpage/myblog/1'/>
          </Switch>
        </Content>
        <Footer className={UserPage.footer}>感谢您的使用!</Footer>
      </Layout>
    </>
    )
  }

  componentDidMount(){
    this.getData();
    this.changeCur();
  }
}
