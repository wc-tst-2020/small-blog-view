import CommentForMe from './index.module.css';
import React, { Component } from 'react';
import { Row, Col, Pagination, List, Divider, Comment, Avatar, Badge, Tooltip, message } from 'antd';
import moment from 'moment';
import api from '../../../request/api';

export default class index extends Component {

    state = {
        // commentList: [{id: '1', userId: '1', userName: '测试1号', avatar: 'https://joeschmoe.io/api/v1/random', content: '测试', commentOf: '测试', dateTime: '2022-1-15 15:22:00', blogId: '1', hasKnown: false}
        //               ,{id: '2', userId: '1', userName: '测试1号', avatar: 'https://joeschmoe.io/api/v1/random', content: '测试', commentOf: '测试', dateTime: '2022-1-16 15:22:00', blogId: '1', hasKnown: true}
        //             ]
        commentList: []
        ,curPage: 1
        ,totalPage: 0
        ,sideWidth: 1
        ,contentWidth: 22
    }

    getData = ()=>{
        api.getCommentListForMe(this.state.curPage, 10)
        .then(res=>{
            if (res.code === 200) {
                var commentList = res.data.list;
                var totalPage =  res.data.totalSize;
                this.setState({commentList, totalPage});
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{
            message.error(error);
        })
    }

    changePage = (curPage) => {
        // console.log(curPage);
        // this.setState({curPage});
        this.props.history.push(`/userpage/commentforme/${curPage}`);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        var page = parseInt(nextProps.match.params.page);
        return {curPage: page};
    }

    render() {
        return (
        <>
            <Row key={this.props.location.pathname}>
                <Col span={this.state.sideWidth}/>
                <Col span={this.state.contentWidth} className={CommentForMe.list}>
                    <List
                    itemLayout="horizontal"
                    dataSource={this.state.commentList}
                    renderItem={item => (
                    <li>
                        <Comment
                            actions={[<span onClick={()=>{window.location.href = `/blog/read/${item.blogId}#${item.id}`}}>回复</span>]}
                            key={item.id}
                            author={
                                <span onClick={()=>{window.location.href = `/authorpage/${item.userId}`}}>
                                    {item.userName}
                                </span>
                            }
                            avatar={
                                <Avatar 
                                onClick={()=>{window.location.href = `/authorpage/${item.userId}`}} 
                                src={item.avatar} 
                                alt={item.userName} 
                                />
                            }
                            datetime={
                                <Tooltip title={moment(item.dateTime).format('YYYY-MM-DD HH:mm:ss')}>
                                    <span>{moment(item.dateTime).fromNow()}</span>
                                </Tooltip>
                            }
                            content={
                                item.hasKnown ? <p>{item.content}</p> : <Badge dot><p>{item.content}</p></Badge>
                            }
                        >
                            <Comment className={CommentForMe.commentOf} 
                            content={
                                <p>{item.commentOf}</p>
                            }
                            />
                        </Comment>
                        <Divider />
                    </li>
                    )}
                    />
                    
                    <div className={CommentForMe.pagination}>
                        <Pagination 
                        showSizeChanger={false} 
                        current={this.state.curPage} 
                        onChange={this.changePage} 
                        total={this.state.totalPage} />
                    </div>
                </Col>
                <Col span={this.state.sideWidth}/>
            </Row>
        </>
        )
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount(){
        this.getData();
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
    }
}
