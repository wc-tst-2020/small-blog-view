import AuthorPage from './index.module.css';
import React, { Component } from 'react'
import { Layout, Row, Col, Avatar, Button, Modal, message } from 'antd'
import { Content, Footer } from 'antd/lib/layout/layout';
import HisBlogs from './HisBlogs'
import MyChat from '../../components/MyChat'
import api from '../../request/api';

export default class index extends Component {

  state = {
    userinfo: {userId:'-1', avatar: 'https://joeschmoe.io/api/v1/random', userName: '未登录'},
    modal: false,
    tokenModal: false
  }
  
  getData = ()=>{
    var userId = this.props.match.params.authorid;
    api.getMySimpleInfo()
    .then(res=>{
      if(res.code === 200) {
        if(res.data.userId === parseInt(userId)) {
          const w = window.open('about:blank')
          w.location.href = '/userpage'
          this.props.history.goBack();
        } else {
          this.getUserInfo();
        }
      } else {
        this.getUserInfo();
      }
    })
    .catch(error=>{
      console.log(error);
      this.getUserInfo();
    })
  }

  getUserInfo = ()=>{
    var userId = this.props.match.params.authorid;
    api.getSimpleUserInfo(userId)
    .then(res=>{
      if(res.code === 200) {
        var userinfo = {
          userId: res.data.userId,
          msgCount: res.data.msgCount,
          avatar: res.data.avatar,
          userName: res.data.userName
        }
        this.setState({userinfo});
      } else {
        message.error(res.msg);
      }
    })
    .catch(error=>{message.error(error)})
  }

  callChatModal = ()=>{
    api.getMySimpleInfo()
    .then(res=>{
      if(res.code === 200) {
        this.setState({modal: true});
      } else {
        if(res.code === 401){
          this.setState({tokenModal: true});
        } else {
          message.error(res.msg);
        }
      }
    })
    .catch(error=>{message.error(error)})
  }

  cancelChatModal = ()=>{
    this.setState({modal: false});
  }

  // 前往登录
  toLogin = ()=>{
    this.setState({tokenModal: false});
    window.location.href = "/identity"
  }

  // 取消登录
  cancelLogin = ()=>{
      this.setState({tokenModal: false});
  }

  render() {
    const {userinfo, modal} = this.state;
    return (
    <>
      <Row>
        <Col span={24} className={[AuthorPage.center, AuthorPage.content]}>
          <Avatar size={128} src={this.state.userinfo.avatar}/>
        </Col>
      </Row>
      <Row>
        <Col span={24} className={[AuthorPage.center, AuthorPage.content]}>
          {this.state.userinfo.userName}
        </Col>
      </Row>
      <Row>
        <Col span={24} className={[AuthorPage.center, AuthorPage.paddingBottom]}>
          <Button type="primary" onClick={this.callChatModal}>私信</Button>
        </Col>
      </Row>

      {/* 信息展示区 */}
      <Layout>
        <Content>
          <HisBlogs key={Math.random()} authorId={userinfo.userId}/>
        </Content>
        <Footer className={AuthorPage.footer}>感谢您的使用!</Footer>
      </Layout>

      {/* 私聊弹窗 */}
      <Modal
        title={`私信：${userinfo.userName}`}
        visible={modal}
        onCancel={this.cancelChatModal}
        footer={null}
        centered = {true}
      >
        <MyChat contactId={userinfo.userId} height={500}/>
      </Modal>

      {/* 登录弹窗 */}
      <Modal 
          title="未登录" 
          visible={this.state.tokenModal} 
          onOk={this.toLogin} 
          onCancel={this.cancelLogin}
          okText="确认"
          cancelText="取消"
      >
          <p>您尚未登录，是否前往登录页面？</p>
      </Modal>       
    </>
    )
  }

  componentDidMount(){
    this.getData();
  }
}
