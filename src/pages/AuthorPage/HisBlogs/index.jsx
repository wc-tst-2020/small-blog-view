import HisBlogs from './index.module.css'
import React, { Component } from 'react';
import { Row, Col, Pagination, message, List } from 'antd';
import BlogListItem from '../../../components/BlogListItem';
import api from '../../../request/api'

export default class index extends Component {

    state = {
        // blogList: [{blogId: 1, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 2, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 3, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 4, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 5, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 6, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 7, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}
        //         ,{blogId: 8, title: "测试", desc: "测试", authorId: 16, authorAvatar: "https://joeschmoe.io/api/v1/random", authorName: "测试", postTime: "2022-1-29 12:13:00"}]
        blogList: []
        ,curPage: 1
        ,totalPage: 0
        ,sideWidth: 1
        ,contentWidth: 22
    };

    getData = (curPage)=>{
        api.getBlogListByAuthor(this.props.authorId, curPage, 10)
        .then(res=>{
            if(res.code === 200) {
                var blogList = res.data.blogList;
                var totalPage = res.data.totalSize;
                this.setState({blogList, totalPage});
            } else {
                message.error(res.msg);
            }
        })
        .catch(error=>{message.error(error)});
    }

    changePage = (curPage) => {
        this.getData(curPage);
        this.setState({curPage});
    }

    render() {
        return (
            <Row>
                <Col span={this.state.sideWidth}/>
                <Col span={this.state.contentWidth} className={HisBlogs.blogList}>
                    <List
                        itemLayout="horizontal"
                        dataSource={this.state.blogList}
                        renderItem={item => (
                            <BlogListItem key={item.blogId} blog={item}/>
                        )}>
                    </List>
                    <div className={HisBlogs.pagination}>
                        <Pagination 
                        showSizeChanger={false} 
                        current={this.state.curPage} 
                        onChange={this.changePage} 
                        total={this.state.totalPage} />
                    </div>
                </Col>
                <Col span={this.state.sideWidth}/>
            </Row>
        );
    }

    onResize = ()=>{
        var curWidth = document.documentElement.clientWidth;
        var sideWidth = 1;
        var contentWidth = 24 - sideWidth * 2;
        if (curWidth >= 1000) {
            sideWidth = 4;
            contentWidth = 24 - sideWidth * 2;
            this.setState({sideWidth,contentWidth});
        } else {
            if(curWidth >= 800){
                sideWidth = 2;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            } else {
                sideWidth = 1;
                contentWidth = 24 - sideWidth * 2;
                this.setState({sideWidth,contentWidth});
            }
        }
    }

    componentDidMount(){
        this.getData(1);
        this.onResize();
        window.addEventListener('resize',this.onResize);
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
    }
}
